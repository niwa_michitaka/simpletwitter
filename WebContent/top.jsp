<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="./js/jquery-3.6.0.js"></script>

	<title>簡易Twitter</title>
	<script type="text/javascript">
		$(function(){
			$("#delete-button").on("click", function() {
				if(confirm("本当にこのつぶやきを削除しますか？")){
					$("span").text("つぶやきを削除しました。");
				}else{
					return false
				}
			});

			$(".message").on({
				mouseenter: function() {
				// マウスが要素上に入った時の処理
				$(this).css("background", "#EEE");
				},
				mouseleave: function() {
				// マウスが要素上から離れた時の処理
				$(this).css("background", "#FFF");
				}
			});
		});
	</script>
</head>
<body>
	<div class="header">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="setting">設定</a>
			<a href="logout">ログアウト</a>
		</c:if>
	</div>
	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				@<c:out value="${loginUser.account}" />
			</div>
			<div class="description">
				<c:out value="${loginUser.description}" />
			</div>
		</div>
	</c:if>
	<div><form action="./" method="get">
		日付：<input type=date name="start" value="${start}" id="start">
		～
		<input type=date name="end" value="${end}" id="end">
		<input type="submit" value="絞り込み">
		</form>
	</div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="form-area">
		<c:if test="${isShowMessageForm}">
			<form action="message" method="post">
				いま、どうしてる？<br />
				<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
				<br /> <input type="submit" value="つぶやく">（140文字まで）
			</form>
		</c:if>
	</div>
	<hr>
	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					<span class="account">
						@<a href="./?user_id=<c:out value="${message.userId}"/> ">
							<c:out value="${message.account}" />
						</a>
					</span> <span class="name"><c:out value="${message.name}" /></span>
				</div>
				<div class="text"><pre><c:out value="${message.text}" /></pre>
				</div>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
				<div class="buttons">
					<c:if test="${message.userId == loginUser.id}">
						<div class="delete">
							<form action="./deleteMessage" method="get" id="delete">
								<input type="hidden" name="deleteMessage" value="${message.id}">
							</form>
						</div>
						<div class="edit">
							<form action="./edit" method="get" id="edit">
								<input type="hidden" name="message_id" value="${message.id}">
							</form>
						</div>
						<div class="buttons">
							<button type="submit" form="delete" class="delete" id="delete-button">削除</button>
							<button type="submit" form="edit" class="edit" id="edit-button">編集</button>
						</div>
					</c:if>
				</div>

				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.messageId}">
					<div class="comment">
						<div class="account-name">
							<span class="account">
								<a>
									<c:out value="${comment.account}" />
								</a>
							</span> <span class="name"><c:out value="${comment.name}" /></span>
						</div>
						<div class="text">
							<pre><c:out value="${comment.text}" /></pre>
						</div>
						<div class="date">
							<fmt:formatDate value="${comment.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
					</div>
					</c:if>
				</c:forEach>
				<div class="form-area">
					<c:if test="${isShowMessageForm}">
						<form action="comment" method="post">
							返信<br />
							<textarea name="comment" cols="100" rows="5" class="comment-box"></textarea>
							<br />
							<input type="hidden" name="message_id" value="${message.id}">
							<input type="submit" value="返信">（140文字まで）
						</form>
						<hr>
					</c:if>
				</div>
			</div>
		</c:forEach>
	</div>
	<div class="copyright">Copyright(c)Michitaka Niwa</div>
</body>
</html>