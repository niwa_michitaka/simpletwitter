package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// 投稿を削除する
	public void delete(String messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			int deleteMessageId = Integer.parseInt(messageId);

			new MessageDao().delete(connection, deleteMessageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(Integer id, String start, String end) {
		final int LIMIT_NUM = 1000;
		final String DEFAULT_DATE = "2022-01-01 00:00:00";

		Connection connection = null;
		try {
			connection = getConnection();

			// statDate処理
			String startDate = null;
			if(!StringUtils.isBlank(start)) {
				startDate = start + " 00:00:00";
			} else {
				startDate = DEFAULT_DATE;
			}
			// endDateの処理
			String endDate = null;
			if(!StringUtils.isBlank(end)) {
				endDate = end + " 23:59:59";
			} else {
				// 現在時刻を取得
				Date today = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				endDate = sdf.format(today);
			}

			// idがnullだったら全件取得する
			// idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);

			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// 投稿を取得する
	public UserMessage select(int id) {

		Connection connection = null;
		try {
			connection = getConnection();
			UserMessage message = new MessageDao().select(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// 投稿を編集する
	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}