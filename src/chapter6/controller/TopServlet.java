package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}
		// JSPからString user_idをrequest.getParameter("user_id")で取得
		String userId = request.getParameter("user_id");
		// idをnullで初期化
		// JSPからuserIdの値が渡ってきていたら
		// 整数型に型変換し、idに代入
		// MessageService.selectへ送る
		Integer id = null;
		if(!StringUtils.isEmpty(userId)) {
			id = Integer.parseInt(userId);
		}

		// 絞り込みの日付を取得
		String startDate = request.getParameter("start");
		String endDate = request.getParameter("end");

		// 投稿・返信の取得
		List<UserMessage> messages = new MessageService().select(id, startDate, endDate);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("start", startDate);
		request.setAttribute("end", endDate);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}