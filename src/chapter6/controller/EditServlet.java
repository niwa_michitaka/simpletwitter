package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String messageId = request.getParameter("message_id");
		UserMessage message = null;

		if(!StringUtils.isBlank(messageId) || messageId.matches("^[0-9]+$")) {
			int id = Integer.parseInt(messageId);
			message = new MessageService().select(id);
		}

		if(message == null) {
			errorMessages.add("不正なパラメータです");
			session.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("./").forward(request, response);
            return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws IOException, ServletException {

    	// 編集後の投稿 text を取得
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("message_id"));

        Message message = new Message();
        message.setText(text);
        message.setId(messageId);

        //投稿内容の確認
        if (!isValid(message.getText(), errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp?message_id=" + message.getId()).forward(request, response);
		}

        // 投稿内容の更新 update
		new MessageService().update(message);

        response.sendRedirect("./");
    }

    // 投稿内容を確認
    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}